import os

import numpy as np
from PIL import Image
#import tensorflow as tf


class ShipDataloader: #(tf.keras.utils.Sequence):
    def __init__(self, path2imgs, path2labels):
        self.x = [os.path.join(path2imgs, f) for f in os.listdir(path2imgs) if not f.startswith('.') and f.endswith('.jpg')]
        self.y = [os.path.join(path2labels, f) for f in os.listdir(path2labels) if not f.startswith('.') and f.endswith('.txt')]
        self.x.sort()
        self.y.sort()
        self.i = 0

    def __len__(self):
        return len(self.y)

    def __getitem__(self, idx):
        img = np.array(Image.open(self.x[idx])).astype(np.float32)
        gt = np.array(self._read_yolo_label_file(self.y[idx])).astype(np.float32)
        return img, gt

    def _read_yolo_label_file(self, filename):
        bbs = []
        with open(filename, 'r') as f:
            for bb in f:
                bbs.append(bb.split())
        return bbs

    def get_next(self):
        a={'images': [np.transpose(self.__getitem__(0)[0], (2,0,1))]}
        print("welp")
        if self.i == 10:
            return
        self.i+=1
        return a


from functools import wraps
from time import time, sleep

def measure_time(f):
    @wraps(f)
    def wrap(*args, **kw):
        ts = time()
        result = f(*args, **kw)
        te = time()
        return result, te-ts
    return wrap

# a = ShipDataloader("../data/imgs", "../data/labels")
# print(len(a))
# for x, y in a:
#     print(y)

def draw_rect(img, x_min, y_min, x_max, y_max, order="xyxy"):
    # if order == "xywh":
    #     _x_min = x_min - x_max / 2
    #     _y_min = y_min - y_max / 2
    #     _x_max = x_min + x_max / 2
    #     _y_max = y_min + y_max / 2

    # convert to integer anyway
    x_min, y_min, x_max, y_max = map(int, np.minimum([x_min, y_min, x_max, y_max], [767]*4))

    if np.shape(img)[0] == 3:
        img = np.transpose(img, (1, 2, 0))

    # draw red rectangle
    red_pixel = (255, 0, 0)
    for i in range(x_min, x_max+1):
        img[y_min, i] = red_pixel
        img[y_max, i] = red_pixel
    for j in range(y_min, y_max+1):
        img[j, x_min] = red_pixel
        img[j, x_max] = red_pixel

    return img
