"""
Backend for inference using the onnxruntime, adapted from MLPerf
"""

import onnxruntime as rt
import numpy as np

from utils import measure_time
from backend import Backend
from postprocess import decode, nms


class BackendOnnxruntime(Backend):
    def __init__(self):
        super(BackendOnnxruntime, self).__init__()
        self.sess = None

    def version(self):
        return rt.__version__

    def name(self):
        """Name of the runtime."""
        return "onnxruntime"

    def image_format(self):
        """image_format. For onnx it is always NCHW."""
        # transpose layer inculded in onnx models
        return "NCHW"

    def load(self, model_path, inputs=None, outputs=None, precision='fp32'):
        """Load model and find input/outputs from the model file."""
        opt = rt.SessionOptions()

        # load CPU or GPU ONNX Backend
        if rt.get_device()=='GPU':
            self.sess = rt.InferenceSession(model_path, opt, providers=['TensorrtExecutionProvider', 'CUDAExecutionProvider', 'CPUExecutionProvider'])
            print("Using Onnxruntime GPU backend!")
        else:  # CPU
            self.sess = rt.InferenceSession(model_path, opt)
            print("Using Onnxruntime CPU backend!")

        # get input and output names
        if not inputs:
            self.inputs = [meta.name for meta in self.sess.get_inputs()]
        else:
            self.inputs = inputs
        if not outputs:
            self.outputs = [meta.name for meta in self.sess.get_outputs()]
        else:
            self.outputs = outputs
        return self

    @measure_time
    def predict(self, feed):
        """Run the prediction."""
        return self.sess.run(self.outputs, {self.inputs[0]: feed})[0]

    def preprocess(self, feed):
        """
        Check for input dimension to be correct, if not transpose input feed
        """
        pass
        channel_idx = 3 if self.input_format() == "NHWC" else 1
        channel_idx -= 1 if len(np.shape(feed)) == 3 else channel_idx  # no batch dimension
        if np.shape(feed)[channel_idx] != 4:  # transpose necessary?
            if len(np.shape(feed)) == 4:  # with batch dimension
                feed = np.transpose(feed, (0, 3, 1, 2))
            else:  # without batch dimension
                feed = np.transpose(feed, (2, 0, 1))
        return feed

    @measure_time
    def postprocess(self, feed):
        """
        Decoding and Non-Maximum-Suppression
        (Sometimes it is necessary to transpose the output, i.e. openvino model)
        """
        # check for right dimensions
        # channel_idx = 2 if self.output_format() == "NPC" else 1
        # if np.shape(feed)[channel_idx] != 6:
        #     feed = np.transpose(feed, (0, 2, 1))

        # decoding and NMS
        feed = decode(feed, (768, 768))
        feed = nms(feed)

        return feed

    def input_format(self):
        """
        N: batch dimension
        H: height
        W: width
        C: channel dimension
        """
        return "NHWC"

    def output_format(self):
        """
        N: batch dimension
        H: height
        W: width
        C: channel dimension
        """
        return "NHWC"
