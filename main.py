"""
This script serves as a little demo to show the capabilities of HighCloud net in fast cloud segmentation.
"""
import numpy as np

from utils import ShipDataloader, draw_rect
from backend_onnxruntime import BackendOnnxruntime

np.random.seed(42)

def detect_ships(path2model: str, image_generator: ShipDataloader, plot_detected_ships: bool):
    model = BackendOnnxruntime()

    print(f"Using the {model.name()} as inference backend!")
    model.load(path2model)

    t_inf, t_post = 0, 0
    for x, y in image_generator:
        x = model.preprocess(x)
        output_raw, dt_inf = model.predict(np.array([x]))
        output, dt_post = model.postprocess(output_raw)
        t_inf += dt_inf
        t_post += dt_post



        # plot last output
        if plot_detected_ships:
            import matplotlib.pyplot as plt

            for rect in output:
                x = draw_rect(x, *rect[:4])

            plt.imshow(x.astype(int), )
            # plt.title("Detected Ships")

            plt.axis("off")
            plt.tight_layout()
            # plt.savefig("detections.png", dpi=100, format='png', bbox_inches='tight', pad_inches=0)

            plt.show()

    print(f"Total inference time for {len(image_generator)} sample(s) was {t_inf + t_post:.1f} ms.")
    print(f"Composed of {t_inf / len(image_generator):.2f} ms inference " \
          f"and {t_post / len(image_generator):.2f} ms postprocessing per sample on average.")


if __name__ == '__main__':
    path2model = "model.onnx"
    path2images = "images/"
    path2labels = "labels/"

    image_generator = ShipDataloader(path2images, path2labels)

    detect_ships(path2model, image_generator, True)
